module.exports = {
  'secret': 'super secret passphrase',
  'database': 'mongodb://localhost:27017/test',
  'port': process.env.PORT || 3000
}

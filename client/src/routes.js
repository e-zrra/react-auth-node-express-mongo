import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import NotFoundPage from './components/pages/not-found-page';

import Register from './components/auth/register';
// import Inbox from './components/dashboard/messaging/inbox';
// import Login from './components/auth/login';

import Dashboard from './components/dashboard/dashboard';
import RequireAuth from './components/auth/require_auth';

// import HomePage from './components/pages/home-page';
// import ContactPage from './components/pages/contact-page';
// import ComponentSamplesPage from './components/pages/component-samples';

export default (
  <Route path="/" component={App}>

    <Route path="register" component={Register} />

      <Route path="dashboard">
        <IndexRoute component={RequireAuth(Dashboard)} />
      </Route>

    <Route path="*" component={NotFoundPage} />
  </Route>
);
